import React from "react";
import { HiExclamationCircle } from "react-icons/hi2";

const AlertNoData = ({ searchInput }) => {
  return (
    <div className="w-full bg-red-500 text-black rounded-xl py-3 mt-6">
      <p className="text-center font-semibold flex justify-center gap-2">
        <span className="self-center">
          <HiExclamationCircle className="w-[25px] h-[25px] text-red-300" />
        </span>{" "}
        <span>
          Tidak ada data yg cocok dengan "<i>{searchInput}</i>"
        </span>
      </p>
    </div>
  );
};

export default AlertNoData;
