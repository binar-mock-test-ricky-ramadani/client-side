import { useNavigate } from "react-router-dom";

const Itemlist = ({ index, name, address, id, searchParams }) => {
  const navigate = useNavigate();

  const hanlderTextSearch = (params) => {
    const TextTrue = ({ char }) => {
      return <span className="text-red-500 font-bold">{char}</span>;
    };
    const TextFalse = ({ char }) => {
      return <span className="text-slate-800">{char}</span>;
    };

    const result = params.split("").map((item, i) => {
      if (searchParams.get("name")?.toLowerCase().includes(item.toLowerCase()))
        return <TextTrue key={i} char={item} />;
      else return <TextFalse key={i} char={item} />;
    });

    return result;
  };

  const handlingChar = (param) => {
    let result = "";
    for (let j = 0; j < param.length; j++) {
      if (j < 11) result += param[j];
      if (j === 11) {
        result += "...";
        break;
      }
    }
    return result;
  };

  return (
    <tr>
      <th className="py-4 text-xs text-slate-800 border-y border-black">
        {index}
      </th>
      <td className="py-4 text-xs text-slate-800 border-y border-black">
        {hanlderTextSearch(handlingChar(name))}
      </td>
      <td className="py-4 text-xs text-slate-800 border-y border-black">
        {handlingChar(address)}
      </td>
      <td className="py-4 text-xs text-slate-800 border-y border-black">
        <button
          type="button"
          className="text-blue-500 border border-blue-500 text-xs px-2 rounded-xl shadow-lg font-semibold"
          onClick={() => navigate(`/students-detail/${id}`)}
        >
          detail
        </button>
      </td>
    </tr>
  );
};

export default Itemlist;
