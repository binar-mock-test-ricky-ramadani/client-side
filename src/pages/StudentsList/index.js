import React, { useEffect, useState } from "react";
import axios from "axios";
import { Link, useNavigate, useSearchParams } from "react-router-dom";
import { useDispatch } from "react-redux";
import BaseLayout from "../../layouts/BaseLayout";
import { BsSearch } from "react-icons/bs";
import { HiUserPlus, HiArrowUturnLeft } from "react-icons/hi2";
import { hideLoading, showLoading } from "../../redux/reducer/loading";
import Itemlist from "./ItemList";
import AlertNoData from "./AlertNoData";

const StudentsList = () => {
  const [fetchData, setfetchData] = useState(true);
  const [dataStudents, setDataStudents] = useState(null);
  const [isDataSearch, setIsDataSearch] = useState(false);
  const [pagination, setPagination] = useState(null);
  const [searchInput, setSearchInput] = useState("");

  const [searchParams, setSearchParams] = useSearchParams();
  const dispatch = useDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    if (searchParams.get("name")) setSearchInput(searchParams.get("name"));
    const pages = searchParams.get("page") || 1;
    const name = searchParams.get("name");
    const fetch = async () => {
      dispatch(showLoading());
      try {
        const response = await axios.get(
          `${process.env.REACT_APP_BASE_URL}/students${
            name ? `?name=${name}&page=${pages}` : `?page=${pages}`
          }`
        );
        setPagination(response.data.pagination);
        setDataStudents(response.data.data);
        dispatch(hideLoading());
      } catch (error) {
        alert(error);
        dispatch(hideLoading());
      }
    };

    if (fetchData) {
      fetch();
      setfetchData(false);
    }
  }, [fetchData]);

  const handleSearch = async (e) => {
    e.preventDefault();
    setIsDataSearch(true);
    navigate(`/students-list?name=${searchInput}&page=1`);
    dispatch(showLoading());
    setfetchData(true);
  };

  return (
    <BaseLayout>
      <div className="flex flex-col text-black pt-10 lg:pt-0">
        {/* Header */}
        <div className="flex flex-col gap-5">
          <div></div>
          <div className="flex justify-end gap-10 pr-2 lg:pr-0">
            <div>
              <Link
                to={"/create-student-form"}
                className="bg-blue-500 py-1 flex gap-1 text-white font-bold px-2 hover:scale-110 text-xs lg:text-base"
              >
                <span>Create Student</span>
                <span className="self-center">
                  <HiUserPlus />
                </span>
              </Link>
            </div>
            <div>
              <form
                method="GET"
                onSubmit={handleSearch}
                className="flex shadow-lg rounded-lg"
              >
                <input
                  type="search"
                  name="name"
                  value={searchInput}
                  onChange={(e) => setSearchInput(e.target.value)}
                  className="bg-white px-2 py-1 text-xs lg:text-sm text-slate-800 font-semibold border-2 border-r-0 border-yellow-500 focus:border"
                  placeholder="search student..."
                />
                <button
                  type="submit"
                  className="bg-yellow-500 px-2 text-xs lg:text-sm rounded-r-md"
                >
                  <BsSearch className="text-blue-500" />
                </button>
              </form>
            </div>
          </div>
          <div>
            <div
              className={`${
                isDataSearch ? "opacity-100" : "opacity-0"
              } flex justify-end pr-2 lg:pr-0`}
            >
              <button
                onClick={() => {
                  setSearchInput("");
                  navigate(`/students-list?page=1`);
                  dispatch(showLoading());
                  setIsDataSearch(false);
                  setfetchData(true);
                }}
                type="button"
                className="flex gap-1 text-black bg-red-500 px-2 py-1 font-bold text-xs hover:scale-110"
              >
                <span>Clear Search</span>
                <span className="self-center">
                  <HiArrowUturnLeft />
                </span>
              </button>
            </div>
          </div>
        </div>
        {/* END HEADER */}
        {/* SECTION TABLE */}
        <div className="h-full p-1 flex flex-col justify-between rounded-xl gap-8">
          <div className="">
            <div className="overflow-x-auto shadow-lg rounded-xl">
              <table className="w-full">
                <thead className="bg-yellow-500 text-slate-800">
                  <tr className="">
                    <th className="p-1 text-sm">No</th>
                    <th className="p-1 text-sm">Name</th>
                    <th className="p-1 text-sm">Address</th>
                    <th className="p-1 text-sm">action</th>
                  </tr>
                </thead>
                <tbody className="bg-white text-center text-slate-800">
                  {dataStudents ? (
                    <>
                      {dataStudents.map((item, index) => {
                        return (
                          <Itemlist
                            searchParams={searchParams}
                            key={index}
                            id={item.id}
                            index={
                              index +
                              1 +
                              8 * (parseInt(searchParams.get("page") || 1) - 1)
                            }
                            name={item.name}
                            address={item.address}
                          />
                        );
                      })}
                    </>
                  ) : null}
                </tbody>
              </table>
            </div>
            {dataStudents ? (
              <>
                {dataStudents.length === 0 ? (
                  <AlertNoData searchInput={searchInput} />
                ) : null}
              </>
            ) : null}
          </div>
          {/* PAGINATION */}
          <div className="flex justify-center">
            {pagination ? (
              <div className="btn-group">
                {pagination?.prev ? (
                  <button
                    type="button"
                    className="bg-transparent border-0 text-white hover:text-slate-200 italic hover:bg-transparent"
                    onClick={() => {
                      if (searchParams.get("name")) {
                        navigate(
                          `/students-list?name=${searchParams.get(
                            "name"
                          )}&page=${pagination.prev}`
                        );
                      } else {
                        navigate(`/students-list?page=${pagination.prev}`);
                      }
                      dispatch(showLoading());
                      setfetchData(true);
                    }}
                  >
                    prev
                  </button>
                ) : null}

                <button className="mx-3 text-white font-bold" type="button">
                  Page {searchParams.get("page") || "1"}/
                  {pagination?.totals_pages}
                </button>
                {pagination?.next ? (
                  <button
                    className="bg-transparent border-0 text-white hover:text-slate-200 italic hover:bg-transparent"
                    type="button"
                    onClick={() => {
                      if (searchParams.get("name")) {
                        navigate(
                          `/students-list?name=${searchParams.get(
                            "name"
                          )}&page=${pagination.next}`
                        );
                      } else {
                        navigate(`/students-list?page=${pagination.next}`);
                      }
                      dispatch(showLoading());
                      setfetchData(true);
                    }}
                  >
                    next
                  </button>
                ) : null}
              </div>
            ) : null}
          </div>
          {/* END PAGINATION */}
        </div>
        {/* END SECTION TABLE */}
      </div>
    </BaseLayout>
  );
};

export default StudentsList;
