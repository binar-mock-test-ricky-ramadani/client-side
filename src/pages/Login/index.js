import React, { useState } from "react";
import axios from "axios";
import Cookies from "js-cookie";
import { HiOutlineLockClosed } from "react-icons/hi2";
import { useNavigate } from "react-router-dom";
import InputWithIcon from "../../components/InputWithIcon";
import FormLayout from "../../layouts/FormLayout";
import { useDispatch } from "react-redux";
import { setAdminRedux } from "../../redux/reducer/admin";
import { hideLoading, showLoading } from "../../redux/reducer/loading";

const Login = () => {
  const [textHeader, setTextHeader] = useState("Login Form");
  const [input, setInput] = useState("");

  const navigate = useNavigate();
  const dispatch = useDispatch();

  const handlerSubmit = async (e) => {
    e.preventDefault();
    dispatch(showLoading());

    try {
      const response = await axios.post(
        `${process.env.REACT_APP_BASE_URL}/login`,
        { password: input }
      );

      dispatch(setAdminRedux(response.data.data));
      Cookies.set("token", response.data.token);
      dispatch(hideLoading());
      navigate("/home");
    } catch (error) {
      if (error.response.status === 404) {
        setTextHeader(error.response.data.message);
        textManipulation();
      }
      dispatch(hideLoading());
    }
  };

  const textManipulation = () => {
    setTimeout(() => {
      setTextHeader("Login Form");
    }, 5000);
  };

  const handlerChange = (e) => {
    setInput(e.target.value);
  };

  return (
    <FormLayout
      textHeader={textHeader}
      textLink={"Sign Up"}
      link={"/register"}
      OnSubmit={handlerSubmit}
    >
      <InputWithIcon
        name={"password"}
        type={"number"}
        placeholder={"Id Login"}
        value={input}
        onChange={handlerChange}
        icon={<HiOutlineLockClosed />}
      />
    </FormLayout>
  );
};

export default Login;
