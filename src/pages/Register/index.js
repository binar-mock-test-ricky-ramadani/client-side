import React, { useState } from "react";
import axios from "axios";
import Cookies from "js-cookie";
import { HiOutlineLockClosed } from "react-icons/hi2";
import { useNavigate } from "react-router-dom";
import InputWithIcon from "../../components/InputWithIcon";
import FormLayout from "../../layouts/FormLayout";
import { dataForm } from "./dataForm";
import { useDispatch } from "react-redux";
import { hideLoading, showLoading } from "../../redux/reducer/loading";

const Register = () => {
  const [textHeader, setTextHeader] = useState("Register Form");
  const [input, setInput] = useState({
    name: "",
    hobby: "",
    password: "",
  });

  const navigate = useNavigate();
  const dispatch = useDispatch();

  const textManipulation = () => {
    setTimeout(() => {
      setTextHeader("Register Form");
    }, 5000);
  };

  const handlerSubmit = async (e) => {
    e.preventDefault();

    if (input.password.length < 4) {
      setTextHeader("Id Login min 4 char!");
      textManipulation();
      return;
    }

    try {
      dispatch(showLoading());
      await axios.post(`${process.env.REACT_APP_BASE_URL}/register`, input);

      navigate("/");
      dispatch(hideLoading());
    } catch (error) {
      if (error.response.status === 301) {
        setTextHeader(error.response.data.message);
        textManipulation();
        dispatch(hideLoading());
      } else {
        alert(error);
        dispatch(hideLoading());
      }
    }
  };

  const handlerChange = (e) => {
    setInput({ ...input, [e.target.name]: e.target.value });
  };

  return (
    <FormLayout
      textHeader={textHeader}
      textLink={"Sign In"}
      OnSubmit={handlerSubmit}
      link={"/"}
    >
      {dataForm !== null && (
        <>
          {dataForm.map((item, i) => {
            return (
              <InputWithIcon
                key={i}
                name={item.name}
                type={item.type}
                placeholder={item.placeholder}
                value={input[item.value]}
                onChange={handlerChange}
                icon={item.icon}
              />
            );
          })}
        </>
      )}
    </FormLayout>
  );
};

export default Register;
