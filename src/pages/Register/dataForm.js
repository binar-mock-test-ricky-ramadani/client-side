import { HiUserCircle, HiBookmarkSquare, HiLockClosed } from "react-icons/hi2";

const dataForm = [
  {
    name: "name",
    type: "text",
    placeholder: "Name",
    value: "name",
    icon: <HiUserCircle />,
  },
  {
    name: "hobby",
    type: "text",
    placeholder: "Hobby",
    value: "hobby",
    icon: <HiBookmarkSquare />,
  },
  {
    name: "password",
    type: "number",
    placeholder: "Id Login",
    value: "password",
    icon: <HiLockClosed />,
  },
];

export { dataForm };
