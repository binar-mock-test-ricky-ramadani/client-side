import axios from "axios";
import moment from "moment/moment";
import React, { useEffect, useState } from "react";
import { HiArrowLeft } from "react-icons/hi2";
import { IoTrashSharp, IoPencil } from "react-icons/io5";
import { useDispatch } from "react-redux";
import { Link, useNavigate, useParams } from "react-router-dom";
import ModalGeneral from "../../components/ModalGeneral";
import { showflashMessage } from "../../redux/reducer/flashMessage";
import { hideLoading, showLoading } from "../../redux/reducer/loading";

const StudentDetail = () => {
  const [showModalDelete, setShowModalDelete] = useState(false);
  const [studentDetail, setStudentDetail] = useState(null);

  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { id } = useParams();

  useEffect(() => {
    const fetch = async () => {
      dispatch(showLoading());
      try {
        const response = await axios.get(
          `${process.env.REACT_APP_BASE_URL}/student/${id}`
        );
        setStudentDetail(response.data.data);
        dispatch(hideLoading());
      } catch (error) {
        if (error.response.status === 404) {
          // navigate ke 404
          navigate("*");
          dispatch(hideLoading());
        }
        dispatch(hideLoading());
      }
    };

    fetch();
  }, []);

  const handlerDelete = async () => {
    dispatch(showLoading());
    try {
      await axios.delete(`${process.env.REACT_APP_BASE_URL}/student/${id}`);
      dispatch(showflashMessage("Succesfully remove student"));
      navigate("/students-list");
    } catch (error) {
      alert(error);
      dispatch(hideLoading());
    }
  };

  const BodyModalDelete = () => {
    return (
      <div className="mt-20">
        <button
          className="w-full bg-red-500 text-black py-2 font-bold hover:scale-y-110 transition duration-300 shadow-lg rounded-lg"
          type="button"
          onClick={handlerDelete}
        >
          delete
        </button>
      </div>
    );
  };

  return (
    <div className="bg-slate-200 min-h-screen flex justify-center text-slate-800">
      {showModalDelete ? (
        <ModalGeneral
          tittle={`Remove student ${studentDetail?.name} ?`}
          body={<BodyModalDelete />}
          setIsModalView={setShowModalDelete}
        />
      ) : null}
      {studentDetail !== null ? (
        <div className="bg-white shadow-lg rounded-lg self-center lg:w-1/2 w-5/6 p-2">
          <div className="pl-5 pt-5 flex">
            <Link className="hover:scale-110" to={"/students-list"}>
              <span className="hover:scale-110 text-black">
                <HiArrowLeft className="w-[25px] h-[25px]" />
              </span>
            </Link>
          </div>
          <div>
            <h2 className="text-center text-2xl font-bold">
              {studentDetail?.name}
            </h2>
          </div>
          <div className="pl-3">Email: {studentDetail?.email}</div>
          <div className="pl-3">Address: {studentDetail?.address}</div>
          <div className="pl-3">Gender: {studentDetail?.gender}</div>
          <div className="pl-3">Place Of Birth: {studentDetail?.pob}</div>
          <div className="pl-3">Age: {studentDetail?.age}th years old.</div>
          <div className="pl-3">Hobby: {studentDetail?.hobby}</div>
          <div className="pl-3 mb-10">
            Date Of Birth: {moment(studentDetail?.dob).format("DD MMMM YYYY")}
          </div>
          <div className="flex justify-evenly mb-10">
            <button
              onClick={() => setShowModalDelete(true)}
              type="button"
              className="bg-red-500 text-black p-3 rounded-full hover:scale-110 transition duration-300 shadow-lg"
            >
              <span>
                <IoTrashSharp />
              </span>
            </button>
            <button
              onClick={() =>
                navigate(`/edit-student-form/${studentDetail?.id}`)
              }
              type="button"
              className="flex gap-1 bg-blue-500 px-3 rounded-lg text-white hover:scale-110 transition duration-300 shadow-lg"
            >
              <span className="self-center">edit</span>{" "}
              <span className="self-center">
                <IoPencil />
              </span>
            </button>
          </div>
          <div className="flex justify-end pr-5">
            <Link
              to={"/students-list"}
              className={"text-blue-500 italic underline"}
            >
              students list
            </Link>
          </div>
        </div>
      ) : null}
    </div>
  );
};

export default StudentDetail;
