import React from "react";
import { useSelector } from "react-redux";
import { HiArrowRight } from "react-icons/hi2";
import BaseLayout from "../../layouts/BaseLayout";
import Hand from "../../assets/img/hand.gif";
import { useInView } from "react-intersection-observer";
import { useNavigate } from "react-router-dom";

const Home = () => {
  const adminInRedux = useSelector((state) => state.adminInRedux.value);
  const { ref: section1ref, inView: section1IsVisible } = useInView();
  const navigate = useNavigate();

  const TextAnimation = ({ char }) => {
    return (
      <span
        className="text-shadow-custom hover:text-red-500 inline-block hover:animate-bounce ease-in-out duration-300"
        style={{ animationIterationCount: 2 }}
      >
        {char}
      </span>
    );
  };

  return (
    <BaseLayout>
      <div
        ref={section1ref}
        className="lg:w-1/2 text-white pt-16 lg:pt-32 lg:pl-16 px-4 lg:px-0 flex flex-col pb-5"
      >
        <div className="mb-10">
          <div className="flex gap-3 mb-4">
            <h1 className="text-6xl font-extrabold parent-text">
              <TextAnimation char={"H"} />
              <TextAnimation char={"i"} />
            </h1>
            <img src={Hand} alt="hand" className="w-[60px] h-[60px] " />
          </div>
          <div>
            <p className="font-extrabold text-xl lg:text-5xl mb-3 parent-text">
              {adminInRedux?.name !== null && (
                <>
                  {adminInRedux.name.split("").map((item, i) => {
                    if (item === " ") return <span key={i}> </span>;
                    if (i === 0) {
                      return (
                        <span
                          key={i}
                          className="text-red-500 text-xl lg:text-5xl"
                          style={{ textShadow: "3px 2px 0px white" }}
                        >
                          {item}
                        </span>
                      );
                    }
                    return <TextAnimation key={i} char={item} />;
                  })}
                </>
              )}
            </p>
          </div>
        </div>
        <div
          className={`${
            section1IsVisible
              ? "rotate-0 translate-x-0 -translate-y-0"
              : "rotate-[360deg] translate-x-[800px] -translate-y-[500px]"
          } transition duration-1000`}
        >
          <button
            onClick={() => navigate("/students-list")}
            type="button"
            className="bg-white group text-red-800 py-2 px-5 rounded font-bold text-sm lg:text-2xl flex gap-2 hover:bg-red-800 hover:text-white transition duration-500 ease-in-out hover:-translate-y-1 hover:scale-110 hover-shadow"
          >
            <span>View Students Data</span>{" "}
            <span className="self-center bg-red-800 text-white p-1 rounded text-sm group-hover:bg-white group-hover:text-red-800 duration-500">
              <HiArrowRight />
            </span>
          </button>
        </div>
      </div>
    </BaseLayout>
  );
};

export default Home;
