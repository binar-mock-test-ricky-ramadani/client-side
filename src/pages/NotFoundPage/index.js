import React from "react";
import { Link } from "react-router-dom";
import Image from "../../assets/img/image-3.jpg";

const NotFoundPage = () => {
  return (
    <div className="relative h-screen overflow-hidden bg-indigo-900">
      <img
        src={Image}
        alt="bg"
        className="absolute object-cover w-full h-full"
      />
      <div className="absolute inset-0 bg-black opacity-25"></div>
      <div className="container relative z-10 flex items-center px-6 py-32 mx-auto md:px-12 xl:py-40">
        <div className="relative z-10 flex flex-col items-center w-full font-mono">
          <h1 className="mt-4 text-5xl font-extrabold leading-tight text-center text-white mb-10">
            You're alone here
          </h1>
          <Link
            to={"/home"}
            className="font-extrabold text-2xl italic text-blue-500 border border-blue-500 rounded-2xl p-1 px-2"
          >
            Back To Home!
          </Link>
          <p className="font-extrabold text-white text-8xl my-44 mb-10 animate-bounce">
            404
          </p>
          <p className="font-extrabold text-white text-3xl mb-10 animate-bounce">
            Page not Found!
          </p>
        </div>
      </div>
    </div>
  );
};

export default NotFoundPage;
