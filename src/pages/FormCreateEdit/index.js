import axios from "axios";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import Background from "../../assets/img/bg-1.jpg";
import {
  hideflashMessage,
  showflashMessage,
} from "../../redux/reducer/flashMessage";
import { hideLoading, showLoading } from "../../redux/reducer/loading";

const FormCreateEdit = ({ type }) => {
  const defaultInput = {
    name: "",
    email: "",
    address: "",
    gender: "",
    age: "",
    hobby: "",
    pob: "",
    dob: "",
  };
  const [input, setInput] = useState(defaultInput);

  const createStudent = "CREATE_STUDENT";
  const editStudent = "EDIT_STUDENT";
  const { id } = useParams();
  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    const fetch = async () => {
      dispatch(showLoading());
      try {
        const response = await axios.get(
          `${process.env.REACT_APP_BASE_URL}/student/${id}`
        );

        const { name, email, address, gender, age, hobby, pob, dob } =
          response.data.data;

        // ASSIGN VALUE TO DATE INPUT
        const newDob = new Date(dob);
        const formatDate =
          newDob.getDate() < 10 ? `0${newDob.getDate()}` : newDob.getDate();
        const formatMonth =
          newDob.getMonth() < 10 ? `0${newDob.getMonth()}` : newDob.getMonth();
        const formattedDate = [
          newDob.getFullYear(),
          formatMonth,
          formatDate,
        ].join("-");

        setInput({
          name,
          email,
          address,
          gender,
          age,
          hobby,
          pob,
          dob: formattedDate,
        });
        dispatch(hideLoading());
      } catch (error) {
        alert(error);
        dispatch(hideLoading());
      }
    };

    if (type === editStudent) {
      fetch();
    }
  }, []);

  const handlerChange = (e) => {
    setInput({ ...input, [e.target.name]: e.target.value });
  };

  const handlerSubmit = async (e) => {
    e.preventDefault();
    dispatch(showLoading());
    if (type === createStudent) {
      try {
        const response = await axios.post(
          `${process.env.REACT_APP_BASE_URL}/create-student`,
          input
        );
        dispatch(showflashMessage("Successfully added student"));
        navigate(`/students-detail/${response.data.data.id}`);
        setInput(defaultInput);
        dispatch(hideLoading());
      } catch (error) {
        if (error.response.status === 401) {
          alert("Something Wrong");
        } else {
          alert(error);
        }

        dispatch(hideLoading());
      }

      return;
    }

    if (type === editStudent) {
      try {
        const response = await axios.post(
          `${process.env.REACT_APP_BASE_URL}/update-student/${id}`,
          input
        );
        dispatch(showflashMessage("Successfully edit student"));
        navigate(`/students-detail/${response.data.data.id}`);
        setInput(defaultInput);
        dispatch(hideLoading());
      } catch (error) {
        if (error.response.status === 404) {
          dispatch(hideLoading());
          alert(error);
        }
      }
    }
  };

  return (
    <div
      className="min-h-screen bg-cover bg-center lg:py-10 py-3 px-2 flex justify-center lg:px-24 text-slate-800"
      style={{ backgroundImage: `url(${Background})` }}
    >
      <div className="bg-slate-200 p-4 rounded shadow-lg self-center">
        <div className="mb-10">
          <h2 className="text-center text-2xl font-bold">
            {type === createStudent ? "Create" : "Edit"} Student
          </h2>
        </div>
        <form action="" method="POST" onSubmit={handlerSubmit}>
          <div className="flex flex-col lg:flex-row gap-5 justify-evenly">
            {/* Dekstop => Left */}
            <div className="flex flex-col gap-2">
              <div className="flex flex-col">
                <label htmlFor="name" className="font-semibold">
                  Name :
                </label>
                <input
                  id="name"
                  required
                  name="name"
                  type="text"
                  value={input.name}
                  onChange={handlerChange}
                  className="bg-white p-1 rounded-lg"
                  placeholder="Name"
                />
              </div>
              <div className="flex flex-col">
                <label htmlFor="email" className="font-semibold">
                  Email :
                </label>
                <input
                  id="email"
                  required
                  name="email"
                  type="email"
                  value={input.email}
                  onChange={handlerChange}
                  className="bg-white p-1 rounded-lg"
                  placeholder="example@gmail.com"
                />
              </div>
              <div className="flex flex-col">
                <label htmlFor="address" className="font-semibold">
                  Address :
                </label>
                <input
                  id="address"
                  name="address"
                  required
                  value={input.address}
                  onChange={handlerChange}
                  type="text"
                  className="bg-white p-1 rounded-lg"
                  placeholder="address..."
                />
              </div>
              <div className="flex flex-col">
                <label htmlFor="gender" className="font-semibold">
                  Gender :
                </label>
                <select
                  name="gender"
                  id="gender"
                  required
                  value={input.gender}
                  onChange={handlerChange}
                  className="bg-white p-1 rounded-lg"
                >
                  <option disabled value={""}>
                    Gender
                  </option>
                  <option value={"male"}>Male</option>
                  <option value={"female"}>Female</option>
                </select>
              </div>
            </div>
            {/* Dekstop => END Left */}
            {/* Dekstop => Right */}
            <div>
              <div className="flex flex-col gap-2">
                <div className="flex flex-col">
                  <label htmlFor="hobby" className="font-semibold">
                    Hobby :
                  </label>
                  <input
                    id="hobby"
                    required
                    value={input.hobby}
                    onChange={handlerChange}
                    name="hobby"
                    type="text"
                    className="bg-white p-1 rounded-lg"
                    placeholder="Hobby"
                  />
                </div>
                <div className="flex flex-col">
                  <label htmlFor="age" className="font-semibold">
                    Age :
                  </label>
                  <input
                    id="age"
                    required
                    name="age"
                    value={input.age}
                    onChange={handlerChange}
                    type="number"
                    className="bg-white p-1 rounded-lg"
                    placeholder="age..."
                  />
                </div>
                <div className="flex flex-col">
                  <label htmlFor="pob" className="font-semibold">
                    Place Of Birth :
                  </label>
                  <input
                    id="pob"
                    name="pob"
                    value={input.pob}
                    required
                    onChange={handlerChange}
                    type="text"
                    className="bg-white p-1 rounded-lg"
                    placeholder="Place of birth"
                  />
                </div>
                <div className="flex flex-col">
                  <label htmlFor="dob" className="font-semibold">
                    Date Of Birth :
                  </label>
                  <input
                    id="dob"
                    required
                    name="dob"
                    value={input.dob}
                    onChange={handlerChange}
                    type="date"
                    className="bg-white p-1 rounded-lg"
                  />
                </div>
              </div>
            </div>
            {/* Dekstop => END Right */}
          </div>
          <div className="flex justify-evenly mt-16 gap-3">
            <button
              onClick={() => {
                navigate("/students-list");
                setInput(defaultInput);
              }}
              className="bg-red-500 text-black font-bold py-1 px-10 rounded-lg hover:scale-110 transition duration-300"
              type="button"
            >
              Cancel
            </button>
            <button
              className="bg-blue-500 text-white font-bold py-1 px-10 rounded-lg hover:scale-110 transition duration-300"
              type="submit"
            >
              Submit
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default FormCreateEdit;
