import { configureStore } from "@reduxjs/toolkit";
import adminReducer from "./reducer/admin.js";
import loadingReducer from "./reducer/loading.js";
import flashMessageReducer from "./reducer/flashMessage.js";

export const store = configureStore({
  reducer: {
    adminInRedux: adminReducer,
    isLoading: loadingReducer,
    flashMessage: flashMessageReducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});
