import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  value: {
    id: null,
    name: "",
    hobby: "",
  },
};

export const adminSlice = createSlice({
  name: "admin",
  initialState,
  reducers: {
    setAdminRedux: (state = initialState, action) => {
      state.value.name = action.payload.name;
      state.value.hobby = action.payload.hobby;
      state.value.id = action.payload.id;
    },
    deleteAdminRedux: (state = initialState) => {
      state.value = {
        id: null,
        name: "",
        hobby: "",
      };
    },
  },
});

// Action creators are generated for each case reducer function
export const { setAdminRedux, deleteAdminRedux } = adminSlice.actions;

export default adminSlice.reducer;
