import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  value: {
    text: "",
    isView: false,
  },
};

export const flashMessageSlice = createSlice({
  name: "flashMessage",
  initialState,
  reducers: {
    showflashMessage: (state = initialState, action) => {
      state.value.text = action.payload;
      state.value.isView = true;
    },

    hideflashMessage: (state = initialState) => {
      state.value.isView = false;
    },
  },
});

// Action creators are generated for each case reducer function
export const { showflashMessage, hideflashMessage } = flashMessageSlice.actions;

export default flashMessageSlice.reducer;
