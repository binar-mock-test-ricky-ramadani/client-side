import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  value: false,
};

export const loadingSlice = createSlice({
  name: "loading",
  initialState,
  reducers: {
    showLoading: (state = initialState) => {
      state.value = true;
    },
    hideLoading: (state = initialState) => {
      state.value = false;
    },
  },
});

// Action creators are generated for each case reducer function
export const { showLoading, hideLoading } = loadingSlice.actions;

export default loadingSlice.reducer;
