import React from "react";
import {
  Routes,
  Route,
  HashRouter,
  BrowserRouter,
  Navigate,
} from "react-router-dom";
import Cookies from "js-cookie";
import Login from "../pages/Login";
import Register from "../pages/Register";
import Home from "../pages/Home";
import StudentsList from "../pages/StudentsList";
import FormCreateEdit from "../pages/FormCreateEdit";
import StudentDetail from "../pages/StudentDetail";
import NotFoundPage from "../pages/NotFoundPage";

function Router() {
  const PrivateRoute = ({ content }) => {
    if (Cookies.get("token")) return content;
    else return <Navigate to="/" />;
  };

  const HandlerLoginRegister = ({ content }) => {
    if (Cookies.get("token")) return <Navigate to="/home" />;
    else return content;
  };

  return (
    <HashRouter>
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route path="/home" element={<Home />} />
        <Route path="/students-list" element={<StudentsList />} />
        <Route path="/students-detail/:id" element={<StudentDetail />} />
        <Route
          path="/create-student-form"
          element={<FormCreateEdit type={"CREATE_STUDENT"} />}
        />
        <Route
          path="/edit-student-form/:id"
          element={<FormCreateEdit type={"EDIT_STUDENT"} />}
        />

        <Route path="*" element={<NotFoundPage />} />
      </Routes>
    </HashRouter>
  );
}

export default Router;
