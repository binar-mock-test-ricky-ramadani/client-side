import React from "react";
import ButtonOpsi from "../../components/ButtonOpsi";
import Navbar from "../../components/Navbar";

const BaseLayout = ({ children }) => {
  return (
    <div
      className="min-h-screen overflow-x-hidden"
      style={{
        background:
          "linear-gradient(270deg, rgba(185,185,185,1) 0%, rgba(88,141,159,1) 47%)",
      }}
    >
      <Navbar />
      <div className="lg:pt-16 lg:px-24">{children}</div>
      <ButtonOpsi />
    </div>
  );
};

export default BaseLayout;
