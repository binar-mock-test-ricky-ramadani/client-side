import React from "react";
import { HiUser } from "react-icons/hi2";
import { Link } from "react-router-dom";
import Background from "../../assets/img/bg-1.jpg";

const FormLayout = ({ children, textHeader, link, textLink, OnSubmit }) => {
  const textHeaderValid = ["Login Form", "Register Form"];

  return (
    <div
      style={{ backgroundImage: `url(${Background})` }}
      className="flex justify-center min-h-screen bg-cover"
    >
      <div
        style={{
          boxShadow: "0px 0px 25px 10px black",
          background: "rgba(4, 29, 23, 0.5)",
        }}
        className="self-center py-3 px-8 rounded-lg flex flex-col gap-5 lg:w-1/4 w-3/4"
      >
        <div className="flex justify-center -mt-8">
          <div
            style={{ boxShadow: "0px 0px 6px 5px black" }}
            className="w-max p-3 rounded-full bg-black text-white"
          >
            <HiUser />
          </div>
        </div>
        <h2
          className={`${
            textHeaderValid.includes(textHeader)
              ? "text-slate-200"
              : "text-red-500"
          } text-2xl text-center font-bold mb-6 animate-bounce`}
        >
          {textHeader}
        </h2>
        <form className="flex flex-col gap-6" method="post" onSubmit={OnSubmit}>
          {children}
          <button
            className="border border-slate-700 py-2 font-semibold hover:shadow-2xl hover:shadow-black text-white"
            type="submit"
          >
            {link === "/" ? "Submit" : "Login"}
          </button>
        </form>
        <div className="flex text-slate-200 gap-1 justify-end text-xs">
          <p>{textLink} </p>
          <Link className="italic text-blue-400 underline" to={link}>
            here!
          </Link>
        </div>
      </div>
    </div>
  );
};

export default FormLayout;
