import React from "react";
import { RotatingLines } from "react-loader-spinner";

const Loading = () => {
  return (
    <div
      style={{ backgroundColor: "rgba(100,100,100,0.7)", zIndex: 1000 }}
      className="absolute top-0 bottom-0 right-0 left-0 flex justify-center z-50"
    >
      <RotatingLines
        strokeColor="green"
        strokeWidth="5"
        animationDuration="0.75"
        width="96"
        visible={true}
      />
    </div>
  );
};

export default Loading;
