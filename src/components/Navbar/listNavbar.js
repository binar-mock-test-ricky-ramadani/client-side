import { HiUserCircle, HiUserGroup } from "react-icons/hi2";

const listNavbar = [
  {
    label: "Profile",
    link: "/home",
    icon: <HiUserCircle />,
  },
  {
    label: "Students",
    link: "/students-list",
    icon: <HiUserGroup />,
  },
];

export { listNavbar };
