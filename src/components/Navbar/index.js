import Cookies from "js-cookie";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { deleteAdminRedux } from "../../redux/reducer/admin";
import ModalGeneral from "../ModalGeneral";
import { listNavbar } from "./listNavbar";

const Navbar = () => {
  const [showModal, setShowModal] = useState(false);

  const navigate = useNavigate();
  const dispatch = useDispatch();
  const adminInRedux = useSelector((state) => state.adminInRedux.value);
  const location = useLocation().pathname;

  const handlerLogout = () => {
    Cookies.remove("token");
    dispatch(deleteAdminRedux());
    navigate("/");
  };

  const ListNavbar = () => {
    return (
      <>
        {listNavbar.map((item, i) => {
          if (location === item.link) {
            return (
              <Link
                className="text-[#00ADB5] border-b border-[#00ADB5] pb-0"
                key={i}
                to={item.link}
              >
                {item.label}
              </Link>
            );
          }

          return (
            <Link
              className="text-slate-100 hover:text-slate-300"
              key={i}
              to={item.link}
            >
              {item.label}
            </Link>
          );
        })}
      </>
    );
  };

  const BodyModal = () => {
    return (
      <div className="flex flex-col pt-10 gap-1">
        <button
          onClick={handlerLogout}
          className="py-3 border border-slate-600 font-bold text-red-500 hover:scale-y-110 transition duration-500"
        >
          logout
        </button>
      </div>
    );
  };

  return (
    <div
      className="fixed top-0 right-0 left-0"
      style={{
        backgroundColor: "rgba(0, 0, 0, 0.4)",
        zIndex: 100,
        maxHeight: "50px",
      }}
    >
      {showModal ? (
        <ModalGeneral
          body={<BodyModal />}
          tittle={"Are you sure you want to logout?"}
          setIsModalView={setShowModal}
        />
      ) : null}
      <div style={{ backdropFilter: "blur(5px)", maxHeight: "50px" }}>
        <div className="flex justify-around py-3">
          <div>
            <p className="lg:text-2xl text-xl font-bold text-[#00ADB5]">
              {adminInRedux.name}
            </p>
          </div>
          <div className="flex gap-4 lg:text-sm text-sm">
            <ListNavbar />
            <button
              onClick={() => setShowModal(true)}
              type="button"
              className="text-slate-100 hover:text-slate-300 -mt-2 lg:-mt-3"
            >
              Logout
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Navbar;
