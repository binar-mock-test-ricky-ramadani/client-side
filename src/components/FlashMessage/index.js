import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { hideflashMessage } from "../../redux/reducer/flashMessage";

const FlashMessage = () => {
  const flashMessage = useSelector((state) => state.flashMessage.value);
  const dispatch = useDispatch();

  useEffect(() => {
    if (flashMessage.isView) {
      setTimeout(() => {
        dispatch(hideflashMessage());
      }, 4000);
    }
  }, [flashMessage.isView, dispatch]);
  return (
    <div
      className={` ${
        flashMessage.isView ? "-translate-y-0" : "-translate-y-[150px]"
      } absolute z-40 w-full flex justify-center top-20 transition duration-500`}
    >
      <div className="px-20 bg-green-500 text-white font-bold lg:text-xl text-xs rounded-lg py-3">
        <h2>{flashMessage.text}</h2>
      </div>
    </div>
  );
};

export default FlashMessage;
