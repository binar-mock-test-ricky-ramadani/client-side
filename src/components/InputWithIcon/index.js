import React from "react";

const InputWithIcon = ({
  name,
  type,
  placeholder,
  value,
  className,
  onChange,
  icon,
}) => {
  return (
    <div className="flex gap-3 border-b-2 border-slate-700 pb-1">
      <span className="self-center">{icon}</span>
      <input
        className={`bg-transparent text-white font-semibold px-2 ${className}`}
        required
        name={name}
        onChange={onChange}
        type={type}
        placeholder={placeholder}
        value={value}
      />
    </div>
  );
};

export default InputWithIcon;
