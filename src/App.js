import "./App.css";
import Router from "./routes";
import { useEffect } from "react";
import Cookies from "js-cookie";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import { setAdminRedux } from "./redux/reducer/admin";
import Loading from "./components/Loading";
import { hideLoading, showLoading } from "./redux/reducer/loading";
import FlashMessage from "./components/FlashMessage";

function App() {
  const adminInRedux = useSelector((state) => state.adminInRedux.value);
  const isLoading = useSelector((state) => state.isLoading.value);

  const dispatch = useDispatch();

  useEffect(() => {
    const fetchAdmin = async () => {
      dispatch(showLoading());
      try {
        const response = await axios.get(
          `${process.env.REACT_APP_BASE_URL}/admin`,
          {
            headers: { Authorization: `${Cookies.get("token")}` },
          }
        );

        dispatch(setAdminRedux(response.data.data));
        dispatch(hideLoading());
      } catch (error) {
        alert(error);
        dispatch(hideLoading());
      }
    };

    if (Cookies.get("token") && !adminInRedux.name) {
      fetchAdmin();
    }
  }, []);

  return (
    <>
      <FlashMessage />
      {isLoading ? <Loading /> : null}
      <Router />
    </>
  );
}

export default App;
