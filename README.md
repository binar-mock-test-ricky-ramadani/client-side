# Link Website

https://ricky-ramadani-binar-mock-test.netlify.app/

id Login yang bisa digunakan :

- 1234
- 4321
- 1111

# link gitlab

https://gitlab.com/binar-mock-test-ricky-ramadani

# resFull APi

- https://binar-mock-test.onrender.com
- https://binar-mock-test.onrender.com/students => list students

# Jawaban

1. Apakah kegunaan JSON pada REST API?

- JSON berguna sebagai wadah data/informasi pada saat REST API melakukan pertukaran data. JSON memiliki keunggulan yang dapat dibaca berbagai macam bahasa pemrograman.

2. Jelaskan bagaimana REST API bekerja?

- REST API bekerja dengan cara memanipulasi data dan saling dipertukarkan antara aplikasi dan server melalui protokol komunikasi, yang biasanya digunakan adalah HTTP.

# Deksripsi

Haloo, Saya Ricky Ramadani dari kelas Full-Stack Web batch 24, facilitator saya Alimuddin Hasan.
Pada web yang saya buat ini saya menerapkan fitur Login dan Register untuk sistem authentication dan authorization sederhana dengan Login yang hanya menggunakan angka minimal 4 charakter. saya juga menerapkan error handling saat id login salah, saat mendaftar ternyata id login kurang dari 4 charakter, dan saat mendaftar ternyata id login sudah ada/dipakai.

Web ini memiliki skema dengan gambaran admin yang dapat menghandel/manipulasi data dari siswa seperti menambah siswa, mengedit siswa, dan mengapus siswa dengan skema CRUD.
Terdapat fitur pagination yang membatasi pengmabilan data yang berlebihan agar tetap memilii performa yang stabil, maksimal pengambilan data adalah 8 siswa.
Di dalam nya juga terdapat fitur "search student" berdasarkan nama dari siswa dengan memanfaatkan query params.

Saya juga sudah menerapkan redux untuk menghandel beberapa fitur seperti component Loading, component Flash Message, dan penyimpana data admin, dan dengan adanya authorization token jwt yang saya simpan di js-cookies, apabila page ter refresh maka akan secara otomatis nge cek token, dan apabila token tersebut ada dan data admin di redux tidak ada maka program akan melakukan pengambilan data admin secara otomatis.

Untuk data yang ada baik data admin dan data siswa telah terintergrasi dengan API dan DBMS dengan menggunakan postgreSQL.
Untuk Source Code saya juga sudah menerapkan reuseble component, jadi cukup membuat 1 component namun dapat di pakai berkali kali.

Terimakasih untuk tim Binar Job connect, Terbaikk👍
